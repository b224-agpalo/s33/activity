fetch('https://jsonplaceholder.typicode.com/todos')
    .then(response => response.json())
    .then(data => {
        data.map((elem) => { console.log(elem.title) })
    })



fetch('https://jsonplaceholder.typicode.com/todos/1')
    .then((response) => response.json())
    .then((data) => console.log(data.title + '-' + data.completed));


fetch('https://jsonplaceholder.typicode.com/todos', {
    method: 'POST',
    headers: {
        'Content-type': 'application/json',
    },
    body: JSON.stringify({
        title: 'Create a new post',
        completed: false

    })

})
    .then((response) => response.json())
    .then((json) => console.table(json));


fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'PUT',
    headers: {
        'Content-type': 'application/json',
    },
    body: JSON.stringify({
        title: 'Update post',
        complete: false


    })

})
    .then((response) => response.json())
    .then((json) => console.table(json));

fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'PATCH',
    headers: {
        'Content-type': 'application/json',
    },
    body: JSON.stringify({
        title: 'Update THIS post',
        complete: false


    })

})
    .then((response) => response.json())
    .then((json) => console.table(json));

fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'DELETE'
});

fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'PUT',
    headers: {
        'Content-type': 'application/json',
    },
    body: JSON.stringify({
        title: 'Update THIS post',
        complete: false,
        dateUpdated: '11-29-22'


    })

})
    .then((response) => response.json())
    .then((json) => console.table(json));